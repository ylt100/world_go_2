import Toast from '../../../miniprogram_npm/@vant/weapp/toast/toast';
import Notify from '../../../miniprogram_npm/@vant/weapp/notify/notify';
import Dialog from '../../../miniprogram_npm/@vant/weapp/dialog/dialog';

const api = require('../../../data/api/api');
const app = getApp();
var QQMapWX = require('../../../data/qqmap-wx-jssdk1.2/qqmap-wx-jssdk.js');
var qqmapsdk;

const date = new Date()
const years = []
const months = []
const bigMonths = [1,3,5,7,8,10,12]
const regularMonths = [4,6,9,11]
const smallMonths = [2]
const days = []
const hours = []
const mins = ['00','10','20','30','40','50']


for (let i = date.getHours()+1; i <= 23; i++) {
  hours.push(i)
}
//大月的最后一天显示下月的日期，根据下月的月份判断下月天数
if(bigMonths.includes(date.getMonth()+1) && date.getDate()==31)
{
  months.push(date.getMonth()+2)
  if(bigMonths.includes(date.getMonth()+2))
  {
    for (let i = 1; i <= 31; i++) {
      days.push(i)
    }
  }
  if(regularMonths.includes(date.getMonth()+2))
  {
    for (let i = 1; i <= 30; i++) {
      days.push(i)
    }
  }
  if(smallMonths.includes(date.getMonth()+2))
  {
    for (let i = 1; i <= 29; i++) {
      days.push(i)
    }
  }
}
//大月的除31号的其余天显示1-31号
if(bigMonths.includes(date.getMonth()+1) && date.getDate()!=31)
{
  months.push(date.getMonth()+1)
  for (let i = date.getDate()+1; i <= 31; i++) {
    days.push(i)
  }
}
//常规月的最后一天
if(regularMonths.includes(date.getMonth()+1) && date.getDate()==30)
{
  months.push(date.getMonth()+2)
  if(bigMonths.includes(date.getMonth()+2))
  {
    for (let i = 1; i <= 31; i++) {
      days.push(i)
    }
  }
  if(regularMonths.includes(date.getMonth()+2))
  {
    for (let i = 1; i <= 30; i++) {
      days.push(i)
    }
  }
  if(smallMonths.includes(date.getMonth()+2))
  {
    for (let i = 1; i <= 29; i++) {
      days.push(i)
    }
  }
}
//常规月的非最后一天显示1-30号
if(regularMonths.includes(date.getMonth()+1) && date.getDate()!=30)
{
  months.push(date.getMonth()+1)
  for (let i = date.getDate()+1; i <= 30; i++) {
    days.push(i)
  }
}
//2月的最后一天
if(smallMonths.includes(date.getMonth()+1) && (date.getDate()==28 || date.getDate()==29))
{
  months.push(date.getMonth()+2)
  //下月比为3月大月
  if(bigMonths.includes(date.getMonth()+2))
  {
    for (let i = 1; i <= 31; i++) {
      days.push(i)
    }
  }
}
//2月的非最后一天
if(smallMonths.includes(date.getMonth()+1) && (date.getDate()!=28 || date.getDate()!=29))
{
  months.push(date.getMonth()+1)
  for (let i = date.getDate()+1; i <= 29; i++) {
    days.push(i)
  }
}

Page({
  data: {
    // 导航头组件所需的参数
    nvabarData: {
      showCapsule: 1, //是否显示左上角图标   1表示显示    0表示不显示
      title: '接送机', //导航栏 中间的标题
      white: true, // 是就显示白的，不是就显示黑的。
      address: '../../packageA/pages/img/airplane.png' // 加个背景 不加就是没有
    },
    pmHeight:app.globalData.pmHeight, //屏幕高度
    ckHeight:app.globalData.ckHeight,  //窗口高度
    ztlHeight:app.globalData.ztlHeight,  //状态栏高度
    top_dhlHeight:app.globalData.top_dhlHeight, //顶部导航栏高度:状态栏+导航栏  自定义的导航栏高度为自己设定（navbar组件中设置）
    bottom_dhlHeight:app.globalData.bottom_dhlHeight, //底部导航栏高度
    //时间选择器
    isSelectedCurrentDate: false,
    isSelectedCurrentFlightDate: false,
    isFligheDate:false,
    year: date.getFullYear(),
    month: months,
    days: days,
    day: days[0],
    hours: hours,
    hour: hours[0],
    mins: mins,
    min: mins[0],
    //-------
    error: '',
    showOneButtonDialog: false,
    oneButton: [{text: '确定'}],
    radio: '2',
    tabs: ['接机', '送机'],
    tabIndex: 0,
    useCarType: 0,
    queryFlight:false,
    isShowNoData:false,
    inputFlightValue:'',
    shows: false,
    minHour: 0,
    maxHour: 24,
    minDate: new Date().getTime() + 1000*60*60*24,
    currentDate: '',
    currentFlightDate: '',
    currentFlightDateCN: '',
    useCarDate:'',
    isShowFlightList:false,
    departureCity: false,
    columns: ['杭州', '宁波', '温州', '嘉兴', '湖州', '深圳'],
    cityList: [],
    showAddress: false,
    isBlue: true,
    isText: true,
    showUser: false,
    destinationCity: false,
    showdestinationCity: false,
    departure: '',
    timedeparture:'',
    destination: '',
    departureText:'从：机场、火车站、旅馆',
    departureValue:'从：机场、火车站、旅馆',
    destinationText:'到：机场、火车站、旅馆',
    destinationValue:'到：机场、火车站、旅馆',
    departureComboText:'从：机场、火车站、旅馆',
    departureComboValue:'从：机场、火车站、旅馆',
    location_dest_id:0,
    location_dept_id:0,
    is_in_mainland1:1,
    timeCity:false,
    orderList: [],
    isAirport: true,
    airports:[{id:0,name:'首都国际机场T3',code:'PEK',city:'北京,中国',longitude: '116.619758',latitude: '40.072776',searchStr:'北京首都国际机场T3PEK'},{id:1,name:'浦东国际机场T1',code:'PVG',city:'上海,中国',longitude: '121.809647',latitude: '31.156584',searchStr:'上海浦东国际机场T1PVG'},{id:2,name:'宝安国际机场T1',code:'SZX',city:'深圳,中国',longitude: '113.819598',latitude: '22.631062',searchStr:'深圳宝安国际机场T1SZX'}],
    selectedLand:'',
    filterAirport:[],
    flightData:'',
    formatter(type, value) {
      if (type === 'year') {
        //return `${value}年`;
        return '';
      } else if (type === 'month') {
        return `${value}月`;
      }else if (type === 'day') {
        return `${value}日`;
      }else if (type === 'hour') {
        return `${value}时`;
      }
      return value;
    },
  },
  cityTap(e){
    console.log('fasdfsdfsdfds');
    console.log(e);
    const cityName=e.detail.cityname;
    this.getAirportsData(cityName)
    this.setData({
      departure: cityName,
      selectCity: false
    })
    //wx.navigateBack();
  },
  onLoad(){
    // 实例化API核心类
    qqmapsdk = new QQMapWX({
      key: 'B55BZ-FQZ6J-VESFE-KQ6JY-7NBKJ-GCFAQ'
    });
    // console.log(app.showRequestInfo());
    const that = this;
    api.getMainLandOpenedCity({
      success: (res) => {
        var citysName = [];
        res.data.data.forEach(element => {
           citysName.push(element.city_name);
        });
        that.setData({
          cityList: res.data.data,
          columns: citysName
        });
      },
    });

    api.airports({
      success: (res) => {
        this.setData({
          airports: res.data.data,
        });
      },
    })
  },
  showUserclose() {
    this.setData({
      showUser: false
    });
  },
  orderList(){
    api.orderList({
      method: 'GET',
      query: {
        user_id: wx.getStorageSync('user').id
      },
      success: (res) => {
        this.setData({
          orderList: res.data.data.data,
        });
      },
    });
  },
  onDepartureChange(event) {

    api.autoComplete({
      method: 'GET',
      query: {
        city: this.data.selectedCity.id,
        content: event.detail
      },
      success: (res) => {
        console.log( res.data.data.tips);
        this.setData({
          departureResults: res.data.data.tips,
        });
      },
    });
  },
  onComboDepartureChange(event){
    api.autoComplete({
      method: 'GET',
      query: {
        city: this.data.selectedComboCity.id,
        content: event.detail
      },
      success: (res) => {
        this.setData({
          departureComboResults: res.data.data.tips,
        });
      },
    });

  },
  onDestinationChange(event) {
    
    api.autoComplete({
      method: 'GET',
      query: {
        city: this.data.selectedDestinationCity.id,
        content: event.detail
      },
      success: (res) => {
        this.setData({
          destinationResults: res.data.data.tips,
        });
      },
    });
    this.setData({
      radio: event.detail, 
    });
  },
  clickAirport(e){
    this.setData({
      selectedTakeoff: this.data.airports[e.currentTarget.dataset.index],
      location_dest_id: this.data.airports[e.currentTarget.dataset.index].location_id,
      selectAirport: false,
      takeoffText: this.data.airports[e.currentTarget.dataset.index].airport.name.length<13?this.data.airports[e.currentTarget.dataset.index].airport.name:this.data.airports[e.currentTarget.dataset.index].airport.name.slice(0,13)+'...',
      takeoffValue: this.data.airports[e.currentTarget.dataset.index].airport.name
    })
  },
  selectDepartureAddress(e){
    console.log(e);
    this.setData({
      selectedDeparture: this.data.departureResults[e.currentTarget.dataset.index],
      showAddress: false,
      departureText: this.data.departureResults[e.currentTarget.dataset.index].name.length<13?this.data.departureResults[e.currentTarget.dataset.index].name:this.data.departureResults[e.currentTarget.dataset.index].name.slice(0,13)+'...',
      departureValue: this.data.departureResults[e.currentTarget.dataset.index].name
    })
  },
  selectComboDepartureAddress(e){
    console.log('sdjflksdj',e)
    this.setData({
      selectedComboDeparture: this.data.departureComboResults[e.currentTarget.dataset.index],
      timeAddress: false,
      departureComboText: this.data.departureComboResults[e.currentTarget.dataset.index].name.length<13?this.data.departureComboResults[e.currentTarget.dataset.index].name:this.data.departureComboResults[e.currentTarget.dataset.index].name.slice(0,13)+'...',
      departureComboValue: this.data.departureComboResults[e.currentTarget.dataset.index].name
    })
  },
  selectDestinationAddress(e){
    console.log(e);
    this.setData({
      selectedDestination: this.data.destinationResults[e.currentTarget.dataset.index],
      showAddress: false,
      destinationText: this.data.destinationResults[e.currentTarget.dataset.index].name.length<13?this.data.destinationResults[e.currentTarget.dataset.index].name:this.data.destinationResults[e.currentTarget.dataset.index].name.slice(0,13)+'...',
      destinationValue: this.data.destinationResults[e.currentTarget.dataset.index].name,
      showdestinationCity: false
    })
  },
  onTabClick(e) {
    let id = e.currentTarget.id;
    this.setData({
      tabIndex: id,
    })
  },
  onTipsClose(){

  },
  selectUseTime() {
    this.setData({
      shows: true,
      isFligheDate: false
    });
  },
  selectFlightDate() {
    this.setData({
      shows: true,
      isFligheDate: true
    });
  },
  selectFlight(e) {
    var dateee = this.data.flightData[e.currentTarget.dataset.index].arrEstimated.replaceAll("T"," ").replaceAll("Z","").replaceAll("-","/");
    console.log('用车时间',this.data.flightData[e.currentTarget.dataset.index])
    console.log('用车时间',new Date(dateee))
    this.setData({
      selectedCity: this.data.flightData[e.currentTarget.dataset.index].arrCity,
      destination: this.data.flightData[e.currentTarget.dataset.index].arrCity,
      dept_date: new Date(dateee),
      dept_date_format: app.datetimeFormat("YY/mm/dd HH:MM",new Date(dateee)),
      useCarDate : app.datetimeFormat("mm月dd日 HH时MM分",new Date(dateee)),
      departureText: this.data.flightData[e.currentTarget.dataset.index].arrPort,
      departureValue: this.data.flightData[e.currentTarget.dataset.index].arrPort,
      isShowFlightList: false,
      queryFlight: false
    })
    var sug = ''
    var iata = this.data.flightData[e.currentTarget.dataset.index].arrCode
    api.getAirportId({
    method: 'GET',
    query: {
      iata: iata
    },
    success: (res) => {
      console.log('机场信息------',res.data.data)
      sug = {
              name: res.data.data.airport.name,
              addr: res.data.data.title,
              city: res.data.data.city.city_name,
              latitude: res.data.data.airport.latitude,
              longitude: res.data.data.airport.longitude,
            };
      this.setData({
        location_dept_id: res.data.data.location_id,
        selectedLand: sug
      });
     },
    })
    // var sug = ''
    // var _this = this
    // var arrPort = this.data.flightData[e.currentTarget.dataset.index].arrCity + this.data.flightData[e.currentTarget.dataset.index].arrPort
    // console.log('根据机场名称获取机场坐标',arrPort)
    // qqmapsdk.geocoder({
    //   //获取表单传入地址
    //   address: arrPort, //地址参数，例：固定地址，address: '北京市海淀区彩和坊路海淀西大街74号'
    //   success: function(res) {//成功后的回调
    //     console.log('getCoordinate',res);
    //     var res = res.result
    //     sug = {
    //       name: res.title,
    //       addr: res.title,
    //       city: res.address_components.city,
    //       district: res.address_components.district,
    //       latitude: res.location.lat,
    //       longitude: res.location.lng,
    //     };
    //     _this.setData({
    //       selectedLand: sug,
    //     })
    //   },
    //   fail: function(error) {
    //     console.error(error);
    //   },
    //   complete: function(res) {
    //     console.log(res);
    //   }
    // })
  },
  confirmUseDate() {
    if(this.data.isFligheDate)
    {
      //标准格式日期
      const standardDate = new Date(this.data.year + "/"+ this.data.month +"/"+ this.data.day +" "+ '00:00:00')
      // if(app.globalData.sysinfo_platform == "devtools"){
      //   this.setData({
      //     currentFlightDate:app.datetimeFormat("YYYY年mm月dd日",standardDate), 
      //   });
      // }
      // else if (app.globalData.sysinfo_platform == "ios") {     
      //   this.setData({
      //     currentFlightDate:app.datetimeFormat("YYYY年mm月dd日",standardDate), 
      //   });
      // } else if (app.globalData.sysinfo_platform == "android") {            
      //   this.setData({
      //     currentFlightDate:app.datetimeFormat("YYYY年mm月dd日",standardDate), 
      //   });
      // }

      this.setData({
        currentFlightDate:app.datetimeFormat("YYYY-mm-dd",standardDate), 
        currentFlightDateCN:app.datetimeFormat("YYYY年mm月dd日",standardDate), 
        isSelectedCurrentFlightDate: true,
        shows:false
      });      
    }
    else
    {
    //标准格式日期
    const standardDate = new Date(this.data.year + "/"+ this.data.month +"/"+ this.data.day +" "+ this.data.hour +":"+ this.data.min +":" + '00')
    this.setData({
      currentDate:this.data.month+'月'+this.data.day+'日'+'   '+this.data.hour+'时'+this.data.min+'分', //用来显示的日期时间
      dept_date: standardDate,
      dept_date_format: app.datetimeFormat("YY/mm/dd HH:MM",standardDate),
      //app.formatDate(standardDate,false),
      //dept_date_format_str: app.formatDate(standardDate,true),
      isSelectedCurrentDate: true,
      shows:false
    });
  }
  },
  onClose() {
    this.setData({
      shows:false,
      isShowFlightList: false
    });
  },
  onInput(event) {
    this.setData({
      currentDate: event.detail,
    });
  },
  onCityChange(event){
    console.log(event)
  },
  onChange(event) {
    this.setData({
      reserveCopyTime: event.detail.getValues(),
    })
  },
  checkData(){
    console.log('起点信息',this.data.selectedLand)
    console.log('终点信息',this.data.selectedDestination)
    console.log('用车时间',this.data.useCarDate)

    if(!this.data.selectedLand || !this.data.selectedDestination || this.data.useCarDate == ''){
      //Notify('请完善路线信息');
      this.setData({
        error: '请完善路线信息'
      })
      return false;
    }else {
      return true;
    }
    //this.showPopup();
  },
  checkData_takeoff(){
    console.log('起点信息',this.data.selectedDeparture)
    console.log('终点信息',this.data.selectedTakeoff)
    console.log('用车时间',this.data.currentDate)

    if(!this.data.selectedDeparture || !this.data.selectedTakeoff || this.data.currentDate == ''){
      //Notify('请完善路线信息');
      this.setData({
        error: '请完善路线信息'
      })
      return false;
    }else {
      return true;
    }
    //this.showPopup();
  },
  onDateCancel(){
    this.setData({
      shows: false
    });
  },
  aboutUs(){
    wx.navigateTo({
      url: '../../packageA/pages/about/about',
    })
  },
  clickToLogin(event){
    if(!wx.getStorageSync('user')){
      wx.navigateTo({
        url: '../auth/auth',
      })
    }
  },
  tz() {

    if(!wx.getStorageSync('token')){
      wx.navigateTo({
        url: '../auth/auth',
      })
      return;
    }

    Toast.loading({
      message: '加载中...',
      forbidClick: true,
    });

    //送机
    const takeoff_dept_latitude = this.data.selectedDeparture ? this.data.selectedDeparture.latitude : '';
    const takeoff_dept_longitude = this.data.selectedDeparture ? this.data.selectedDeparture.longitude : '';
    const takeoff_dest_location = this.data.location_dest_id;
    const takeoff_dest_latitude = this.data.selectedTakeoff ? this.data.selectedTakeoff.latitude : '';
    const takeoff_dest_longitude = this.data.selectedTakeoff ? this.data.selectedTakeoff.longitude : '';
    //接机
    const land_dept_latitude = this.data.selectedLand?this.data.selectedLand.latitude : '';
    const land_dept_longitude = this.data.selectedLand?this.data.selectedLand.longitude : '';
    const land_dest_latitude = this.data.selectedDestination ? this.data.selectedDestination.latitude : '';
    const land_dest_longitude = this.data.selectedDestination ? this.data.selectedDestination.longitude : '';
    const land_dept_location = this.data.location_dept_id;
    //const takeoff_dest_location = this.data.location_dest_id;
    const target_city = this.data.selectedCity;
    const target_dept_latitude = this.data.tabIndex == 0?land_dept_latitude:takeoff_dept_latitude;
    const target_dept_longitude = this.data.tabIndex == 0?land_dept_longitude:takeoff_dept_longitude;
    const target_dest_latitude = this.data.tabIndex == 0?land_dest_latitude:takeoff_dest_latitude;
    const target_dest_longitude = this.data.tabIndex == 0?land_dest_longitude:takeoff_dest_longitude;

    //const target_city = this.data.tabIndex == 0?this.data.selectedCity:this.data.selectedComboCity;
    //const target_dept = this.data.tabIndex == 0?dept_loc:combo_dept_loc;
    //const target_dest = this.data.tabIndex == 0?dest_loc:combo_dept_loc;

    let query = {
      // area_id: target_city.id,
      // dept_lat: target_dept_latitude,
      // dept_lng: target_dept_longitude,
      // service_type: this.data.tabIndex == 0 ? 3 : 4,
      // date: this.data.tabIndex == 0? this.data.useCarDate : this.data.currentDate,
    }
    if(this.data.tabIndex == 0)
    {
      query = {
        area_id: target_city.id,
        dest_lat: target_dest_latitude,
        dest_lng: target_dest_longitude,
        service_type: 3,
        date: this.data.useCarDate,
        departure_id: land_dept_location
      }
      console.log('jsfdlkjfsdofs',query)
    }
    else
    {
      query = {
        area_id: target_city.id,
        dept_lat: target_dept_latitude,
        dept_lng: target_dept_longitude,
        service_type: 4,
        date: this.data.currentDate,
        destination_id: takeoff_dest_location
      }
    }
    console.log('查询接口参数',query)
    
    this.data.requestPriceParams = query;
    api.getPrices({
      query,
      success: (res) => {
        Toast.clear();
        if(res.data.code == 201){
          // Dialog.alert({
          //   message: '该路线暂未开通,咨询客服可定制 0755-86665622',
          //   theme: 'round-button',
          // }).then(() => {
          //   // on close
          // });
          this.setData({
            showOneButtonDialog: true
          })
          return;
        }
        this.setData({
          priceList : res.data.data.price_list,
          route : res.data.data.route
        })

        wx.navigateTo({
          url: '../worldCarlist/worldCarlist?charterType=1',
        })
      },
    });
  },
  addTakeoff() {
    this.setData({
      selectAirport: true
    });
  },
  closeTakeoff() {
    this.setData({
      selectAirport: false
    });
  },
  addLand() {
    this.setData({
      queryFlight: true
    });
  },
  closeLand() {
    this.setData({
      queryFlight: false
    });
  },
  selectCity() {
    this.setData({
      departureCity: true,
      showAddress: false
    })
  },
  closeSelectCity() {
    this.setData({
      selectCity: false
    })
  },
  selectCityTap() {
    this.setData({
      selectCity: true,
      showAddress: false
    })
  },
  selectDesCity() {
    this.setData({
      destinationCity: true,
      showdestinationCity: false
    })
  },
  selectTimeCity() {
    this.setData({
      timeCity: true,
      timeAddress: false
    })
  },
  addStart() {
    this.setData({
      showAddress: true
    })
    this.getsuggest_load(this.data.timedeparture);
  },
  addbtns() {
    this.setData({
      //destinationCity: true
      showdestinationCity: true
    });
    this.getsuggest_load(this.data.destination);
  },
  //出发地选择取消
  onCancel() {
    this.setData({
      departureCity: false,
    });
  },
  book_takeoff(){
    console.log('book');
    if(this.checkData_takeoff())
    {
      this.tz();
    }
  },
  book_land(){
    console.log('book');
    if(this.checkData())
    {
      this.tz();
    }
  },
  CityConfirm(event) {
    const {
      picker,
      value,
      index
    } = event.detail;
    console.log(`当前值：${value}, 当前索引：${index}`);
    console.log(this.data.cityList[index])
    console.log(event.detail.value)
    const that = this;
    that.setData({
      selectCity: false,
      departure:event.detail.value
    });
    that.getAirportsData(this.data.cityList[index].id)
    /*this.setData({
      selectedCity: this.data.cityList[index]
    })*/
  },
  onConfirm(event) {
    const {
      picker,
      value,
      index
    } = event.detail;
    console.log(`当前值：${value}, 当前索引：${index}`);
    console.log(event.detail.value)
    const that=this
    that.setData({
      departureCity: false,
      showAddress: true,
      departure:event.detail.value
    });
    this.getsuggest_load(event.detail.value)
    /*this.setData({
      selectedCity: this.data.cityList[index]
    })*/
  },
  //按时长出发地选择取消
  timeonCancel() {
    this.setData({
      timeCity: false,
    });
  },

  timeonConfirm(event) {
    const {
      picker,
      value,
      index
    } = event.detail;
    console.log(`当前值：${value}, 当前索引：${index}`);
    console.log(event.detail.value)
    const that=this
    that.setData({
      timeCity: false,
      timeAddress: true,
      timedeparture:event.detail.value,
      departure:event.detail.value
    });
    this.setData({
      selectedComboCity: this.data.cityList[index]
    })
    this.getsuggest_load(event.detail.value)
  },
  //目的地选择取消
  desonCancel() {
    this.setData({
      destinationCity: false
    });
  },

  desonConfirm(event) {
    const {
      picker,
      value,
      index
    } = event.detail;
    console.log(`当前值：${value}, 当前索引：${index}`);
    this.setData({
      destinationCity: false,
      showdestinationCity: true,
      destination:event.detail.value,
      departure:event.detail.value
      //selectedDestinationCity: this.data.cityList[index]
    });
    this.getsuggest_load(event.detail.value)
  },
  out() {
    this.setData({
      showdestinationCity: false
    });
  },
  outadd() {
    this.setData({
      showAddress: false
    });
  },
  timeout(){
    this.setData({
      timeAddress:false
    });
   
  },
  linebtn() {
    this.setData({
      isBlue: true
    });
    this.setData({
      isText: true
    });


  },
  linebtns() {
    this.setData({
      isBlue: false
    });
    this.setData({
      isText: false
    });

    this.orderList();
  },
  userBtn() {
    this.setData({
      showUser: true
    });
  },
  onShow(){
    if(wx.getStorageSync('user')){
      this.setData({
        userInfo: wx.getStorageSync('user')
      })
      var _this = this;
      //调用定位方法
      _this.getUserLocation();
    }
  },
  //定位方法

  getUserLocation() {

  var _this = this;

  wx.getSetting({

    success: (res) => {

      // res.authSetting['scope.userLocation'] == undefined    表示 初始化进入该页面

      // res.authSetting['scope.userLocation'] == false    表示 非初始化进入该页面,且未授权

      // res.authSetting['scope.userLocation'] == true    表示 地理位置授权
      console.log(res.authSetting['scope.userLocation'])
      if (res.authSetting['scope.userLocation'] != undefined && res.authSetting['scope.userLocation'] != true) {

        //未授权

        wx.showModal({

          title: '请求授权当前位置',

          content: '需要获取您的地理位置，请确认授权',

          success(res) {

            if (res.cancel) {

              //取消授权

              wx.showToast({

                title: '拒绝授权',

                icon: 'none',

                duration: 1000

              })

            } else if (res.confirm) {

              //确定授权，通过wx.openSetting发起授权请求

              wx.openSetting({
                success(res) {
                  if (res.authSetting["scope.userLocation"] == true) {
                    wx.showToast({
                      title: '授权成功',
                      icon: 'success',
                      duration: 1000
                    })
                    //再次授权，调用wx.getLocation的API
                    _this.geo();
                  } else {
                    wx.showToast({
                      title: '授权失败',
                      icon: 'none',
                      duration: 1000
                    })
                  }
                }
              })
            }
          }
        })
      } else if (res.authSetting['scope.userLocation'] == undefined) {
       //用户首次进入页面,调用wx.getLocation的API
        _this.geo();
      }
      else {
        console.log('授权成功')
        //调用wx.getLocation的API
        _this.geo();
      }
    }
  })
},         
// 获取定位城市
geo() {

  var _this = this;
  wx.getLocation({
    type: 'gcj02',
    success: function (res) {
      console.log('当前定位坐标',res)
      var latitude = res.latitude
      var longitude = res.longitude
      qqmapsdk.reverseGeocoder({
    //位置坐标，默认获取当前位置，非必须参数
    /**
     * 
     //Object格式
      location: {
        latitude: 39.984060,
        longitude: 116.307520
      },
    */
    /**
     *
     //String格式
      location: '39.984060,116.307520',
    */
    poi_options: 'policy=3;radius=500;address_format=short',
    location: latitude + ',' + longitude || '', //获取表单传入的位置坐标,不填默认当前位置,示例为string格式
    get_poi: 1, //是否返回周边POI列表：1.返回；0不返回(默认),非必须参数
    success: function(res) {//成功后的回调
      var res = res.result;
      var mks = [];
      /**
       *  当get_poi为1时，检索当前位置或者location周边poi数据并在地图显示，可根据需求是否使用
       *
          for (var i = 0; i < result.pois.length; i++) {
          mks.push({ // 获取返回结果，放到mks数组中
              title: result.pois[i].title,
              id: result.pois[i].id,
              latitude: result.pois[i].location.lat,
              longitude: result.pois[i].location.lng,
              iconPath: './resources/placeholder.png', //图标路径
              width: 20,
              height: 20
          })
          }
      *
      **/
      //当get_poi为0时或者为不填默认值时，检索目标位置，按需使用
      mks.push({ // 获取返回结果，放到mks数组中
        name: res.pois[0].title,
        city: res.pois[0].ad_info.city,
        id: 0,
        district: res.pois[0].ad_info.district,
        addr:res.pois[0].address,
        latitude: res.pois[0].location.lat,
        longitude: res.pois[0].location.lng
      });

      _this.setData({ //设置markers属性和地图位置poi，将结果在地图展示
        departure: mks[0].city,
        destination: mks[0].city,
        timedeparture: mks[0].city,
        selectedDeparture:  mks[0],
        departureText: mks[0].name.length<13?mks[0].name : mks[0].name.slice(0,13) + '...',
        departureValue: mks[0].name,
        selectedComboDeparture:  mks[0],
        departureComboText: mks[0].name.length<13?mks[0].name : mks[0].name.slice(0,13) + '...',
        departureComboValue: mks[0].name,
        markers: mks
      });
    },
    fail: function(error) {
      console.error(error);
    },
    complete: function(res) {
      for(var i=0;i<_this.data.cityList.length;i++){
        if(_this.data.cityList[i].city_name===_this.data.markers[0].city){
          _this.setData({ 
            selectedCity: _this.data.cityList[i],
            selectedComboCity: _this.data.cityList[i]
          })
        }
      }
    }
  })
    }
  })
},

//根据地址获取坐标
getCoordinate(adress) {
  var _this = this;
  //调用地址解析接口
  qqmapsdk.geocoder({
    //获取表单传入地址
    async:false,
    address: adress, //地址参数，例：固定地址，address: '北京市海淀区彩和坊路海淀西大街74号'
    success: function(res) {//成功后的回调
      console.log('getCoordinate',res);
      var sug = ''
      var res = res.result
      sug = {
        name: res.title,
        addr: res.title,
        city: res.address_components.city,
        district: res.address_components.district,
        latitude: res.location.lat,
        longitude: res.location.lng,
      };

      console.log('caonima',sug)
      _this.setData({ //设置suggestion属性，将关键词搜索结果以列表形式展示
        selectedLand: sug
      });
      console.log('caonidaye',_this.data.selectedLand)
      // var latitude = res.location.lat;
      // var longitude = res.location.lng;
      // 根据地址解析在地图上标记解析地址位置
      // _this.setData({ // 获取返回结果，放到markers及poi中，并在地图展示
      //   poi: { //根据自己data数据设置相应的地图中心坐标变量名称
      //     latitude: latitude,
      //     longitude: longitude
      //   }
      // });
    },
    fail: function(error) {
      console.error(error);
    },
    complete: function(res) {
      console.log(res);
    }
  })
},
  navigateToAirport(e){
    Dialog.alert({
      message: '该功能正在内测,敬请期待',
      closeOnClickOverlay:true,
      theme: 'round-button'
    }).then(() => {
      // on close
    });
  },
  navigateToKjc(e){
    wx.navigateToMiniProgram({
      appId: 'wx374a4f7345c11f21',
      path: 'pages/Index/index?id=123',
      success(res) {
        // 打开成功
      }
    })
  },
  navigateToPT(e){
    wx.navigateToMiniProgram({
      appId: 'wx374a4f7345c11f21',
      path: 'pages/FreightUseIndex/index?id=123',
      success(res) {
        // 打开成功
      }
    })
  },
  timeBtn(){
    this.setData({
      //timeCity: true
      timeAddress: true
    });
    this.getsuggest_load(this.data.timedeparture);
  },
  //在Page({})中使用下列代码
//数据回填方法
backfill: function (e) {
  console.log('数据回填',e)
  var id = e.currentTarget.id;
  for (var i = 0; i < this.data.suggestion.length;i++){
    if(i == id){
      this.setData({
        backfill: this.data.suggestion[i].title
      });
    }  
  }
},

//触发关键词输入提示事件
getsuggest: function(e) {
  console.log('出发关键词',e)
  var _this = this;
  console.log('城市关键词',_this.data.departure)

  //调用关键词提示接口
  qqmapsdk.getSuggestion({
    //获取输入框值并设置keyword参数
    keyword: e.detail!='' ? e.detail : _this.data.departure , //用户输入的关键词，可设置固定值,如keyword:'KFC'
    region:_this.data.departure, //设置城市名，限制关键词所示的地域范围，非必填参数
    success: function(res) {//搜索成功后的回调
      console.log(res);
      var sug = [];
      for (var i = 0; i < res.data.length; i++) {
        sug.push({ // 获取返回结果，放到sug数组中
          name: res.data[i].title,
          id: res.data[i].id,
          addr: res.data[i].address,
          city: res.data[i].city,
          district: res.data[i].district,
          latitude: res.data[i].location.lat,
          longitude: res.data[i].location.lng
        });
      }
      _this.setData({ //设置suggestion属性，将关键词搜索结果以列表形式展示
        departureResults: sug,
        destinationResults: sug,
        departureComboResults: sug
      });
    },
    fail: function(error) {
      console.error(error);
    },
    complete: function(res) {
      console.log(res);
    }
  })
},
//触发关键词输入提示事件
getsuggest_load(para_keyword) {
  var _this = this;
  //调用关键词提示接口
  qqmapsdk.getSuggestion({
    //获取输入框值并设置keyword参数
    keyword: para_keyword , //用户输入的关键词，可设置固定值,如keyword:'KFC'
    region:_this.data.departure, //设置城市名，限制关键词所示的地域范围，非必填参数
    success: function(res) {//搜索成功后的回调
      console.log(res);
      var sug = [];
      for (var i = 0; i < res.data.length; i++) {
        sug.push({ // 获取返回结果，放到sug数组中
          name: res.data[i].title,
          id: res.data[i].id,
          addr: res.data[i].address,
          city: res.data[i].city,
          district: res.data[i].district,
          latitude: res.data[i].location.lat,
          longitude: res.data[i].location.lng
        });
      }
      _this.setData({ //设置suggestion属性，将关键词搜索结果以列表形式展示
        departureResults: sug,
        destinationResults:sug,
        departureComboResults: sug
      });
    },
    fail: function(error) {
      console.error(error);
    },
    complete: function(res) {
      console.log(res);
    }
  })
},
bindChange: function (e) {
  const val = e.detail.value
  console.log(val)
  if(this.data.days[val[0]] == date.getDate()+1)
  {
    hours.length=0
    for (let i = date.getHours()+1; i <= 23; i++) {
      hours.push(i)
    }
    this.setData({
      hours: hours
    })
  }
  else
  {
    hours.length=0
    for (let i = 0; i <= 23; i++) {
      hours.push(i)
    }
    this.setData({
      hours: hours
    })
  }
  this.setData({
    month: this.data.month,
    day: this.data.days[val[0]],
    hour: this.data.hours[val[1]],
    min: this.data.mins[val[2]]
  })
},
bindKeyInput: function (e) {
  this.setData({
    inputFlightValue: e.detail.value
  })
},
queryFlightList() {
  if(this.data.inputFlightValue == ''){
    //Notify('请填写航班号');
    this.setData({
      error: '请填写航班号'
    })
    return;
  }
  else{
    if(this.data.currentFlightDate == ''){
      //Notify('请选择起飞日期');
      this.setData({
        error: '请选择起飞日期'
      })
      return;
    }
    else{
      wx.showLoading({
        title: '查询中',
        icon: 'loading', 
        duration: 30000
      })
      this.setData({
        isShowFlightList:true,
        isShowNoData:false
      })
      var sug = []
      //调用API
      let queryParam ={
        flightNo:this.data.inputFlightValue,
        date:this.data.currentFlightDate
      }
      console.log(queryParam)
      api.getFlightinfo({
        data: queryParam,
        success: res => {
          console.log('查询航班信息',res)
          let dataList = res.data.result.output.result; //获取到的数据
          
          dataList.forEach((item) => {
            item.arrEstimated = item.arrScheduled //将到达时间赋给该变量，用作用车时间
            item.depActual = app.datetimeFormat("mm月dd日",new Date(item.depScheduled))
            item.arrActual = app.datetimeFormat("mm月dd日",new Date(item.arrScheduled))
            item.depScheduled = item.depScheduled.substring(11,16); //截取时间
            item.arrScheduled = item.arrScheduled.substring(11,16); //截取时间
            item.depPort = item.depPort.replace(item.depCity,''); //截取掉城市
            item.arrPort = item.arrPort.replace(item.arrCity,''); //截取掉城市
          })
          if(dataList.length == 0)
          {
            this.setData ({
              isShowNoData: true
            })
          }
          this.setData ({
            flightData : dataList,
          })
          wx.hideLoading()
          console.log(this.data.flightData)
        }
      });
    }
  }
},
getAirportsData(cityID)
{
  const that = this;
  api.airportsById({
    method: 'GET',
    query: {
      city_id: cityID
    },
    success: (res) => {
      console.log('机场数据',res.data.data)
      that.setData({
        airports: res.data.data,
      });
    },
  });
},
filterArray : function(e){
  console.log('关键字',e)
  const that=this
  var sug = [];
  this.data.airports.filter(item => {
      // 如果list(name)包含key(输入框) => true
      console.log(item)
      if(item.airport.name.includes(e.detail)){
          // 返回item
          sug.push(item);
     }
  })
  console.log('筛选过后的数据',sug)
  that.setData({ //设置suggestion属性，将关键词搜索结果以列表形式展示
    airports: sug
  });
},
tapDialogButton(e) {
  this.setData({
      showOneButtonDialog: false
  })
}
})
