const app = getApp();
const api = require('../../../data/api/api');
import Notify from '../../../miniprogram_npm/@vant/weapp/notify/notify';
import Toast from '../../../miniprogram_npm/@vant/weapp/toast/toast';

Page({
  data: {
    // 导航头组件所需的参数
    nvabarData: {
      showCapsule: 1, //是否显示左上角图标   1表示显示    0表示不显示
      title: '订单详情', //导航栏 中间的标题
      white: true, // 是就显示白的，不是就显示黑的。
      address: '../../assets/images/statusBar-background.png' // 加个背景 不加就是没有
    },
    // 导航头的高度
    height: app.globalData.top_dhlHeight,
    pmHeight:app.globalData.pmHeight, //屏幕高度
    ckHeight:app.globalData.ckHeight,  //窗口高度
    ztlHeight:app.globalData.ztlHeight,  //状态栏高度
    top_dhlHeight:app.globalData.top_dhlHeight, //顶部导航栏高度:状态栏+导航栏  自定义的导航栏高度为自己设定（navbar组件中设置）
    bottom_dhlHeight:app.globalData.bottom_dhlHeight, //底部导航栏高度
    isShowLuggage:false,
    isShowRule:false,
    isShowRefund:false,
    isShowInfomation:false,
     // 展开折叠
     selectedFlag: [false],
  },
  onLoad(){
    const data = getApp().getPrevPage().data;
    const stackPageData = getApp().getPageInStack(3).data;
    console.log('车辆列表数据',data);
    console.log('aaaaaaa',stackPageData);

    this.setData({
      dept_date_text:data.dept_date_text,  //用来显示的时间格式
      dept_date_value:data.dept_date_value,  //用来下单的时间格式
      dept_date: data.dept_date,  //Tue Jun 08 2021 16:00:00 GMT+0800 (中国标准时间)
      //dept_date_format: stackPageData.dept_date_format,  //2021-06-08 16:00:00
      refund_date:app.formatDateTime(data.dept_date.setDate(data.dept_date.getDate() - 1),false),
      
      departure:stackPageData.departureValue,
      departureComboText:stackPageData.departureComboValue,
      carService: data.selectedService,
      luggage_info: data.selectedService.luggage_num,
      //tips: data.selectedService.extra_tip,
      route_id: stackPageData.route.route_id,
      tabIndex: stackPageData.tabIndex,
      useCarType: stackPageData.useCarType,
      car_id: data.selectedService.id,
      car_package_id: data.car_package_id,
      price: data.price,
      combo_id:data.service_combo_id,
      combo_name:data.service_combo_name,
      car_package_name:data.car_package_name
    })
    if(stackPageData.isAirport)
    {
      if(data.tabIndex == 0)
      {
        this.setData({ 
          destination:stackPageData.destinationValue,
        });
      }
      else
      {
        this.setData({ 
          destination:stackPageData.takeoffValue,
        });
      }
    }
    else
    {
      this.setData({ 
        destination:stackPageData.destinationValue,
       });
    }
  },
  showLuggage() {
    this.setData({ isShowLuggage: true });
  },
  showRule() {
    this.setData({ isShowRule: true });
  },
  showRefund() {
    this.setData({ isShowRefund: true });
  },
  agreeUser() {
    this.setData({ isShowInfomation: true });
  },
  onClose() {
    this.setData({ show: false });
  },
  selectPassenger() {
    wx.navigateTo({
      url: '../../../pages/user/user',
    })
  },
  goToPay(e){
    this.createOrder();
  },
  createOrder(){
    console.log('写进订单表的数据',this.data)
    if(!this.data.selectPasseger){
      Notify('请选择乘车人');
      return;
    }

    Toast.loading({
      message: '正在预约...',
      forbidClick: true,
    });
    
    const that = this;
    var orderData ={}
    if(this.data.useCarType == 0)  //按线路
    {
      console.log('按线路数据')
      orderData = {
      "car_id": this.data.carService.car_id,
      "custom_id": 1,
      "dept_date_time": this.data.dept_date_value,
      "dept_loc": this.data.departure,
      "dest_loc": this.data.destination,
      "passenger_mobile": this.data.selectPasseger.mobile,
      "passenger_name": this.data.selectPasseger.contact,
      "route_id": this.data.route_id,
      "service_id": this.data.service_id,
      "type": 1,
      "user_id": wx.getStorageSync('user').id,
      "price_id": this.data.carService.price_id,
      "price": this.data.carService.price,
      "amount": this.data.carService.price,
      "car_type_id": this.data.carService.id,
      source: 2
      }
    }
    else  //按套餐
    {
      console.log('按套餐数据')
      orderData = {
        "car_id": this.data.carService.car_id,
        "custom_id": 1,
        "dept_date_time": this.data.dept_date_value,
        "dept_loc": this.data.departureComboText,
        "dest_loc": this.data.departureComboText,
        "passenger_mobile": this.data.selectPasseger.mobile,
        "passenger_name": this.data.selectPasseger.contact,
        "route_id": this.data.route_id,
        "service_id": this.data.service_id,
        "type": 1,
        "user_id": wx.getStorageSync('user').id,
        "price_id": this.data.carService.price_id,
        "price": this.data.carService.price,
        "amount": this.data.carService.price,
        "car_type_id": this.data.carService.id,
        "combo_id": this.data.combo_id,
        source: 2
        }
    }
    api.createOrder({
      method:'POST',
      data: orderData,
      success: res => {
        console.log('创建订单成功时的数据',res)
        Toast.clear();
        if(res.data.code != 200){
          Notify(res.data.msg);
        }
        
          that.payOrder(res.data.data.order_id);
        
      }
    });
  },
  payOrder(order_id){
    Toast.loading({
      message: '准备支付...',
      forbidClick: true,
    });
    api.payOrder({
      method:'POST',
      data: {
        order_id: order_id,
        customer_id: wx.getStorageSync('user').id,
        pay_type:1
      },
      success: res => {
        Toast.clear();
        const response = res.data.data.jsApiRequirement;
        wx.requestPayment({
          'timeStamp': response.timeStamp,
          'nonceStr': response.nonceStr,
          'package': response.package,
          'signType': response.signType,
          'paySign': response.paySign,
          'success': function (res) {
            wx.navigateBack({
              delta: 3,
            })
          },
          'fail': function (res) {
         
          },
        })
      }
    });
  },
  // 展开折叠选择  
  changeToggle:function(e){
    console.log('展开折叠选择',e)
    var index = e.currentTarget.dataset.index;
    if (this.data.selectedFlag[index]){
      this.data.selectedFlag[index] = false;
    }else{
      this.data.selectedFlag[index] = true;
    }

    this.setData({
      selectedFlag: this.data.selectedFlag
    })
  },
  confirm() {
    this.setData({
      isShowLuggage:false,
      isShowRule:false,
      isShowRefund:false,
      isShowInfomation:false
    });
  }
  
 })