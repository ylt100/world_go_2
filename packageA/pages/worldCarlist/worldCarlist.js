const app = getApp();
const api = require('../../../data/api/api');
import Toast from '../../../miniprogram_npm/@vant/weapp/toast/toast';
import Dialog from '../../../miniprogram_npm/@vant/weapp/dialog/dialog';
Page({
  data: {
    // 导航头组件所需的参数
    nvabarData: {
      showCapsule: 1, //是否显示左上角图标   1表示显示    0表示不显示
      title: '选择服务', //导航栏 中间的标题
      white: true, // 是就显示白的，不是就显示黑的。
      address: '../../assets/images/statusBar-background.png' // 加个背景 不加就是没有
    },
    // 导航头的高度
    top_dhlHeight: app.globalData.top_dhlHeight,
    pmHeight:app.globalData.pmHeight, //屏幕高度
    active:0,
    currentTab:0,
    show: false,
    carTypeInfos:[],
    combos:[{name:'包车半天'},{name:'市内3小时'},{name:'市内5小时'},{name:'包车半天'},{name:'市内3小时'},{name:'市内5小时'}],
    scrollActive:0,//控制当前显示盒子 
    charterType:1,//1 路线接送 2 按时包车
  },
  onLoad(option){
     const prevPageData = getApp().getPrevPage().data;
     console.log('首页数据',prevPageData);
     if(prevPageData.isAirport)
     {
      this.setData({
         carTypeInfos: prevPageData.priceList,
         dept_date: prevPageData.dept_date,
         dept_date_text: prevPageData.tabIndex == 0 ? prevPageData.useCarDate : prevPageData.currentDate,
         dept_date_value: prevPageData.dept_date_format,
         departureText:prevPageData.departureText,
         departureValue:prevPageData.departureValue,
         requestPriceParams: prevPageData.requestPriceParams,
         tabIndex: prevPageData.tabIndex,
         useCarType:prevPageData.useCarType
      })
      if(prevPageData.tabIndex == 0)
      {
       this.setData({
         destinationText:prevPageData.destinationText,
         destinationValue:prevPageData.destinationValue,
       })      
      }
      else
      {
       this.setData({
         destinationText:prevPageData.takeoffText,
         destinationValue:prevPageData.takeoffValue,
       })  
      }
     }
     else
     {
     this.setData({
        carTypeInfos: prevPageData.priceList,
        dept_date: prevPageData.dept_date,
        dept_date_text: prevPageData.currentDate,
        dept_date_value: prevPageData.dept_date_format,
        departureText:prevPageData.departureText,
        departureValue:prevPageData.departureValue,
        destinationText:prevPageData.destinationText,
        destinationValue:prevPageData.destinationValue,
        departureComboText:prevPageData.departureComboText,
        departureComboValue:prevPageData.departureComboValue,
        requestPriceParams: prevPageData.requestPriceParams,
        tabIndex: prevPageData.tabIndex,
        useCarType:prevPageData.useCarType
     })
     
     this.setData({
       charterType:option.charterType,
       combos: prevPageData.combos
     })

     if(prevPageData.useCarType == 1){
      this.data.service_combo_id = prevPageData.selected_combo.id
      this.data.service_combo_name = prevPageData.selected_combo.name
     }
    }
  },
  showPopup() {
    this.setData({ show: true });
  },

  onClose() {
    this.setData({ show: false });
  },
  switchNav: function (e) {
    console.log(e)
    var page = this;
    var id = e.target.id;
    if (this.data.currentTab == id) {
      return false;
    } else {
      page.setData({
        currentTab: id
      });
    }
    page.setData({
      active: id
    });
  },
  changeCombo:function(e){
    
    let query = this.data.requestPriceParams;

    query.service_combo_id = this.data.combos[e.currentTarget.dataset.index].id;
    
  
    Toast.loading({
      message: '加载中...',
      forbidClick: true,
    });
    api.getPrices({
      query,
      success: (res) => {
        this.data.service_combo_id = this.data.combos[e.currentTarget.dataset.index].id;
        this.data.service_combo_name = this.data.combos[e.currentTarget.dataset.index].name;
        Toast.clear();
        if(res.data.code == 201){
          Dialog.alert({
            message: '该路线暂未开通,咨询客服可定制 0755-86665622',
            theme: 'round-button',
          }).then(() => {
            // on close
          });
          return;
        }
        
        if(res.data.data.price_list.length == 0){
          Dialog.alert({
            message: '该服务套餐暂无车型报价',
            theme: 'round-button',
          }).then(() => {
            // on close
          });
          return;
        }

        this.setData({
          carTypeInfos : res.data.data.price_list,
          route : res.data.data.route,
          scrollActive: e.currentTarget.dataset.index
        })

      },
    });
  },
  checkOrder(e){
    console.log(e);
    const serviceIndex = e.target.dataset.serviceIndex;
    const carIndex = e.target.dataset.index;
    this.setData({
      selectedService: this.data.carTypeInfos[serviceIndex].cars[carIndex],
      car_package_id:this.data.carTypeInfos[serviceIndex].car_package_id,
      car_package_name:this.data.carTypeInfos[serviceIndex].car_package_name,
      price:this.data.carTypeInfos[serviceIndex].price,
    });
    wx.navigateTo({
      url: '../order/order'
    })
  },
 })