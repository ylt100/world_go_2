// pages/my/my.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
        // 导航头组件所需的参数
        nvabarData: {
          showCapsule: 0, //是否显示左上角图标   1表示显示    0表示不显示
          title: '我的', //导航栏 中间的标题
          white: true, // 是就显示白的，不是就显示黑的。
          address: '../../assets/images/my-back@3x.png' // 加个背景 不加就是没有
        },
        // 导航头的高度
        height: app.globalData.top_dhlHeight
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.clickToLogin()

  },
  onShow(){
    this.onLoad()
    // if(wx.getStorageSync('user')){
    //   this.setData({
    //     userInfo: wx.getStorageSync('user')
    //   })
    // }
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },
  clickToLogin(event){
    if(!wx.getStorageSync('user')){
      wx.navigateTo({
        url: '../auth/auth',
      })
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },
  myOrder(){
    wx.switchTab({
      url: '../../pages/orderlist/orderlist',
      success:function () {
        var page = getCurrentPages().pop();
        if (page == undefined || page == null) return;
        page.onLoad();   //重新刷新页面
      }
    })
  },
  myWallet(){
    wx.navigateTo({
      url: '../../packageA/pages/wallet/wallet',
    })
  },
  contactUs(){
    wx.navigateTo({
      url: '../../packageA/pages/contact/contact',
    })  
  },
  aboutUs(){
    wx.navigateTo({
      url: '../../packageA/pages/about/about',
    })
  },
  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})