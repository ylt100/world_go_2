// pages/orderlist/orderlist.js
const api = require('../../data/api/api');
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 导航头组件所需的参数
    nvabarData: {
      showCapsule: 0, //是否显示左上角图标   1表示显示    0表示不显示
      title: '订单列表', //导航栏 中间的标题
      white: true, // 是就显示白的，不是就显示黑的。
      address: '../../assets/images/statusBar-background.png' // 加个背景 不加就是没有
    },
    // 导航头的高度
    height: app.globalData.top_dhlHeight

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.orderList();
  },
  orderList(){
    api.orderList({
      method: 'GET',
      query: {
        user_id: wx.getStorageSync('user').id
      },
      success: (res) => {
        console.log('订单列表数据',res)
        this.setData({
          orderList: res.data.data.data,
        });
      },
    });
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.onLoad()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})