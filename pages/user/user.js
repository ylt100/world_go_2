const api = require('../../data/api/api');
const app = getApp();
import Notify from '../../miniprogram_npm/@vant/weapp/notify/notify';

Page({
  data: {
    // 导航头组件所需的参数
    nvabarData: {
      showCapsule: 1, //是否显示左上角图标   1表示显示    0表示不显示
      title: '选择联系人', //导航栏 中间的标题
      white: true, // 是就显示白的，不是就显示黑的。
      address: '../../assets/images/statusBar-background.png' // 加个背景 不加就是没有
    },
    // 导航头的高度
    height: app.globalData.top_dhlHeight,
    radio: '2',
    tabs: ['路线接送', '按时长'],
    tabIndex: 0,
    shows:false,
    minHour: 0,
    maxHour: 24,
    minDate: new Date().getTime(),
    currentDate: new Date().getTime(),
    show:false,
    user:[],
    action_type: 1,//1 新增 2更新
  namevalue:'',
  inputName:'',
  inputMobile:'',
  },
  
  getPassengerList() {
    api.passengerList({
      query:{
        customer_id: wx.getStorageSync('user').id,
      },
      success: (res) => {
        this.setData({
          user : res.data.data
        })
      }
    });
  },
  onLoad(e){
    this.getPassengerList();
  },
  addPassenger() {
    const that = this;

    if(this.data.inputName == '' || this.data.inputMobile == ''){
      Notify('请填写完整信息');
    }

    api.addPassenger({
      method:'POST',
      data:{
        customer_id: wx.getStorageSync('user').id,
        contact: this.data.inputName,
        mobile: this.data.inputMobile
      },
      success: (res) => {
        that.setData({ show: false });
        that.getPassengerList();
      },
    });
  },
  updatePassenger() {
    const that = this;
    api.updatePassenger({
      method:'POST',
      data:{
        id: this.data.updatePassengerId,
        contact: this.data.inputName,
        mobile: this.data.inputMobile
      },
      success: (res) => {
        that.setData({ show: false });
        that.getPassengerList();
      },
    });
  },
  tapUpdate(e){
    console.log('tapupdate',e)
    const user = this.data.user[e.target.dataset.index];
    this.setData({
      inputName: user.contact,
      inputMobile: user.mobile,
      updatePassengerId:user.id,
      action_type: 2,
      show: true
    });
  },
  selectPassenger(e){
    console.log('selectpassenger',e);
    const prevPage = getApp().getPrevPage();
    prevPage.setData({
      selectPasseger:this.data.user[e.currentTarget.dataset.index]
    })
    wx.navigateBack({
      delta: 1,
    })
  },
  showPopup() {
  
    this.setData({
      action_type: 1, 
      show: true 
    });
  },

  onClose() {
    this.setData({ show: false });
  },
  tapAddPassenger(){
    if(this.data.action_type == 1){
      this.addPassenger();
    }else{
      this.updatePassenger();
    }
  },
  userNameInput(e){
      this.setData({
        inputName: e.detail.value
      })
  },
  userMobileInput(e){
    this.setData({
      inputMobile: e.detail.value
    })
  },
  updata(event){
    this.setData({ show: true,
    namevalue:event.currentTarget.dataset.item.name
    });
    console.log(this.data.namevalue)
    console.log(event.currentTarget.dataset.item)
    console.log(event.currentTarget.dataset.item.name)

  },
 })