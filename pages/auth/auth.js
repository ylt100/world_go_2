// pages/auth/auth.js
const api = require('../../data/api/api');
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
        // 导航头组件所需的参数
        nvabarData: {
          showCapsule: 1, //是否显示左上角图标   1表示显示    0表示不显示
          title: '登录', //导航栏 中间的标题
          white: true, // 是就显示白的，不是就显示黑的。
          address: '../../assets/images/statusBar-background.png' // 加个背景 不加就是没有
        },
        // 导航头的高度
        height: app.globalData.top_dhlHeight

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },
  bindGetUserInfo(res) {
    console.log('用户信息',res);
    let avatarUrl = this.data.avatarUrl
    let nickName = this.data.nickName
    let gender = this.data.gender
    let encryptedData = this.data.tCode
    let iv = this.data.iv
    let code = this.data.loginCode
    let that = this
    wx.getUserInfo({
      success: function (res) {
        that.setData({
          avatarUrl: res.userInfo.avatarUrl,
          gender: res.userInfo.gender,
          nickName: res.userInfo.nickName,
          encryptedData: res.encryptedData,
          iv: res.iv
        })
        wx.login({
          success(res) {
            if (res.code) {
              //发起网络请求 
              that.requestLogin(that.data.nickName, that.data.avatarUrl, that.data.gender, res.code,
                that.data.encryptedData, that.data.iv)
            } else {
              console.log('登录失败！' + res.errMsg)
            }
          }
        })
      }
    })
    if (!res.detail.userInfo) {
      //用户按了拒绝按钮

      wx.showModal({

        title: '警告',

        content: '您点击了拒绝授权，将无法下单，请授权之后再进入!!!',

        showCancel: false,

        confirmText: '返回授权',

        success: function (res) {
          if (res.confirm) {

            console.log('用户点击了“返回授权”');

          }

        }

      });
    }
  },
  requestLogin(nick_name, avatar_url, gender, code, encryptedData, iv) {
    api.userLogin({
      method: 'POST',
      data: {
        nick_name,
        avatar_url,
        gender,
        code,
        encryptedData,
        iv,
        source:2
      },
      success: res => {
        if (res.data.code == 200) {
          console.log(res.data.data.user)
          let that = this
          let token = that.data.token
          that.setData({
            token: res.data.data.token
          })
          console.log(that.data.token)
          wx.setStorageSync('token',res.data.data.token)
          wx.setStorageSync('user',res.data.data.user)
          wx.setStorageSync('mobile', res.data.data.user.mobile)
            
          wx.navigateBack({
            delta: 1,
          })
        } else if (res.data.code == 500) {

        }
        
      }
    });
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})