const debugUrl = 'http://test.car.ylt100.cn/api/client';
const productURL = 'https://api.worldgo.ylt100.cn/api/client';
const http_flight_3rd = 'https://way.jd.com/';
const isDebug = false;
const apiUrl = isDebug ? debugUrl : productURL;

const wxRequest = (params, url) => {
  wx.request({
    url,
    method: params.method || 'GET',
    data: params.data || {},
    header: {
      'Content-Type': 'application/json',
      'Accept': 'application/x.api.v1+json'
    },
    success(res) {
      if (params.success) {
        params.success(res);
      }
    },
    fail(res) {

      if (params.fail) {
        params.fail(res);
      }
    },
    complete(res) {
      if (params.complete) {
        params.complete(res);
      }
    },
  });
};

const wxRequest_external = (params, url) => {
  wx.request({
    url,
    method: params.method || 'GET',
    data: params.data || {},
    header: {
      'Content-Type': 'application/json',
    },
    success(res) {
      if (params.success) {
        params.success(res);
      }
    },
    fail(res) {

      if (params.fail) {
        params.fail(res);
      }
    },
    complete(res) {
      if (params.complete) {
        params.complete(res);
      }
    },
  });
};
//获取已开通的城市
const getMainLandOpenedCity = (params) => {
  wxRequest(params, `${apiUrl}/region/cities`);
};

//获取市内按时包车价格和过滤条件
const getPackagePriceFilter = (params) => {
  wxRequest(params, `${apiUrl}/service/day_charter/price?area_id=${params.query.area_id}&coo_ids=${params.query.coo_ids}
  &date=${params.query.date}&type=${params.query.type}`);
};

//获取市内单程接送价格和过滤条件
const getOneWayPriceFilter = (params) => {
  wxRequest(params, `${apiUrl}/service/oneway/price?area_id=${params.query.area_id}&dept_lat_lng=${params.query.dept_lat_lng}&dest_lat_lng=${params.query.dest_lat_lng}&date=${params.query.date}&structure=nesting`);
};

//获取报价
const getPrices = (params) => {
  let url = "";
  if(params.query.service_combo_id){
     url = `${apiUrl}/service/price/check?dept_lat=${params.query.dept_lat}&dept_lng=${params.query.dept_lng}
     &dest_lat=${params.query.dest_lat}&dest_lng=${params.query.dest_lng}&service_type=${params.query.service_type}&date=${params.query.date}&service_combo_id=${params.query.service_combo_id}`
  }else{
    if(params.query.service_type == 4)
    {
      url = `${apiUrl}/service/price/check?dept_lat=${params.query.dept_lat}&dept_lng=${params.query.dept_lng}
      &destination_id=${params.query.destination_id}&service_type=${params.query.service_type}&date=${params.query.date}`
    }
    else if(params.query.service_type == 3)
    {
      url = `${apiUrl}/service/price/check?dest_lat=${params.query.dest_lat}&dest_lng=${params.query.dest_lng}
      &departure_id=${params.query.departure_id}&service_type=${params.query.service_type}&date=${params.query.date}`
    }
    else
    {
    url = `${apiUrl}/service/price/check?dept_lat=${params.query.dept_lat}&dept_lng=${params.query.dept_lng}
    &dest_lat=${params.query.dest_lat}&dest_lng=${params.query.dest_lng}&service_type=${params.query.service_type}&date=${params.query.date}`
    }
  }
  wxRequest(params, url);
};

//获取报价
const getPricesBy = (params) => {
  wxRequest(params, `${apiUrl}/service/price/check?dept_lat=${params.query.dept_lat}&dept_lng=${params.query.dept_lng}
  &dest_lat=${params.query.dest_lat}&dest_lng=${params.query.dest_lng}&service_type=${params.query.service_type}&date=${params.query.date}`);
};


//生成包车订单
const createOrder = (params) => {
  wxRequest(params, `${apiUrl}/order/create`);
};

//获取地址输入提示
const autoComplete = (params) => {
  wxRequest(params, `${apiUrl}/map/autocomplete?city=${params.query.city}&content=${params.query.content}`);
};

//新增用户
const addUser = (params) => {
  wxRequest(params, `${apiUrl}/auth/mp_login`);
};

//支付接口
const payOrder = (params) => {
  wxRequest(params, `${apiUrl}/order/pay`);
};

//用户登录
const userLogin = (params) => {
  wxRequest(params, `${apiUrl}/auth/mp_login`);
};

//添加乘车人
const addPassenger = (params) => {
  wxRequest(params, `${apiUrl}/user/addPassenger`);
};

//更新乘车人
const updatePassenger = (params) => {
  wxRequest(params, `${apiUrl}/user/updatePassenger`);
};

//乘车人列表
const passengerList = (params) => {
  wxRequest(params, `${apiUrl}/user/allPassenger?customer_id=${params.query.customer_id}`);
};

//订单列表
const orderList = (params) => {
  wxRequest(params, `${apiUrl}/order/list?user_id=${params.query.user_id}&order_type=1&is_cross_border=0`);
};

//获取服务套餐
const combos = (params) => {
  wxRequest(params, `${apiUrl}/service/combos?city_id=${params.query.city_id}`);
};

//获取所有机场数据
const airports = (params) => {
  wxRequest(params, `${apiUrl}/region/airports`);
};

//按城市ID获取机场数据
const airportsById = (params) => {
  wxRequest(params, `${apiUrl}/region/airports?city_id=${params.query.city_id}`);
};

//获取航班信息
const getFlightinfo = (params) => {
  var flightApiUrl =`${http_flight_3rd}apemesh/fdaq?appkey=58af0771555f06d907b252412dcdcc7a`
  wxRequest_external(params, flightApiUrl);
};

//根据iata获取机场信息
const getAirportId = (params) => {
  wxRequest(params, `${apiUrl}/region/airport/find?iata=${params.query.iata}`);
};



module.exports = {
  addUser,
  autoComplete,
  getPackagePriceFilter,
  getOneWayPriceFilter,
  getMainLandOpenedCity,
  createOrder,
  payOrder,
  userLogin,
  addPassenger,
  passengerList,
  updatePassenger,
  orderList,
  getPrices,
  combos,
  getFlightinfo,
  airports,
  airportsById,
  getAirportId
};
