// app.js
App({
  onLaunch() {
    // 展示本地存储能力
    const logs = wx.getStorageSync('logs') || []
    logs.unshift(Date.now())
    wx.setStorageSync('logs', logs)

    // 登录
    wx.login({
      success: res => {
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
      }
    })
    // 获取用户信息
    wx.getSetting({
      success: res => {
        if (res.authSetting['scope.userInfo']) {
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
          wx.getUserInfo({
            success: res => {
              // 可以将 res 发送给后台解码出 unionId
              this.globalData.userInfo = res.userInfo

              // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
              // 所以此处加入 callback 以防止这种情况
              if (this.userInfoReadyCallback) {
                this.userInfoReadyCallback(res)
              }
            }
          })
        }
      }
    })
    wx.getSystemInfo({
      success: res => {
        console.log('系统信息',res)
        let pxToRpxScale = 750 / res.windowWidth;
        this.globalData.pmHeight = res.screenHeight * pxToRpxScale  //816px  812px
        this.globalData.ztlHeight = res.statusBarHeight * pxToRpxScale //37px  44px
        this.globalData.ckHeight = res.windowHeight * pxToRpxScale  //765px  730px
        this.globalData.bottom_dhlHeight = res.screenHeight  * pxToRpxScale - res.windowHeight  * pxToRpxScale  //appleX 82px
        this.globalData.top_dhlHeight = (res.statusBarHeight * 2 + 20) * pxToRpxScale
        this.globalData.height = res.statusBarHeight  //apple x 44px
        this.globalData.sysinfo_platform = res.platform
      }
    })
  },
  //获取当前页面
  getCurrentPage() {
    const pages = getCurrentPages();
    const currentPage = pages[pages.length - 1];
    return currentPage;
  },
  //获取上一级页面
  getPrevPage() {
    const pages = getCurrentPages();
    const currentPage = pages[pages.length - 2];
    return currentPage;
  },
  //获取指定层级的页面
  getPageInStack(delta) {
    const pages = getCurrentPages();
    const currentPage = pages[pages.length - delta];
    return currentPage;
  },
  //datetime format
  datetimeFormat(fmt, date) {
    let ret;
    const opt = {
        "Y+": date.getFullYear().toString(),        // 年
        "m+": (date.getMonth() + 1).toString(),     // 月
        "d+": date.getDate().toString(),            // 日
        "H+": date.getHours().toString(),           // 时
        "M+": date.getMinutes().toString(),         // 分
        "S+": date.getSeconds().toString()          // 秒
        // 有其他格式化字符需求可以继续添加，必须转化成字符串
    }
    for (let k in opt) {
        ret = new RegExp("(" + k + ")").exec(fmt);
        if (ret) {
            fmt = fmt.replace(ret[1], (ret[1].length == 1) ? (opt[k]) : (opt[k].padStart(ret[1].length, "0")))
        };
    };
    return fmt;
  },
  formatDate(datetime,is_has_chinese) {
    var date = new Date(datetime);//时间戳为10位需*1000，时间戳为13位的话不需乘1000
    var year = date.getFullYear(),
        month = ("0" + (date.getMonth() + 1)).slice(-2),
        sdate = ("0" + date.getDate()).slice(-2),
        hour = ("0" + date.getHours()).slice(-2),
        minute = ("0" + date.getMinutes()).slice(-2),
        second = ("0" + date.getSeconds()).slice(-2);
    // 拼接
    if(is_has_chinese){
      var result = month +"月"+ sdate +"日"+ " " + hour +":"+ minute +":" + second;
    }else{
    var result = year + "-"+ month +"-"+ sdate +" "+ hour +":"+ minute +":" + second;
    }
    // 返回
    return result;
  },
  formatDateTime(datetime,is_has_chinese) {
    var date = new Date();//时间戳为10位需*1000，时间戳为13位的话不需乘1000
    var year = date.getFullYear(),
        month = ("0" + (date.getMonth() + 1)).slice(-2),
        sdate = ("0" + date.getDate()).slice(-2),
        hour = ("0" + date.getHours()).slice(-2),
        minute = ("0" + date.getMinutes()).slice(-2);
        //second = ("0" + date.getSeconds()).slice(-2);
    // 拼接
    if(is_has_chinese){
      var result = month +"月"+ sdate +"日"+ hour +":"+ minute;
    }else{
    var result = year + "-"+ month +"-"+ sdate +" "+ hour +":"+ minute;
    }
    // 返回
    return result;
  },
  /*getPageHeight(){
  let systemInfo = wx.getSystemInfoSync()
  // px转换到rpx的比例
  let pxToRpxScale = 750 / systemInfo.windowWidth;
  // 状态栏的高度
  let ktxStatusHeight = systemInfo.statusBarHeight * pxToRpxScale
  // 导航栏的高度
  let navigationHeight = 44 * pxToRpxScale
  // window的高度
  let ktxWindowHeight = systemInfo.windowHeight * pxToRpxScale
  // 屏幕的高度
  let ktxScreentHeight = systemInfo.screenHeight * pxToRpxScale
  // 底部tabBar的高度
  let tabBarHeight = ktxScreentHeight - ktxStatusHeight - navigationHeight - ktxWindowHeight
  },*/
  globalData: {
    userInfo: null,
    share: false, // 分享默认为false
    height: 0, // 顶部高度
    pmHeight:0, //屏幕高度
    ckHeight:0,  //窗口高度
    ztlHeight:0,  //状态栏高度
    top_dhlHeight:0, //顶部导航栏高度:状态栏+导航栏  自定义的导航栏高度为自己设定（navbar组件中设置）
    bottom_dhlHeight:0, //底部导航栏高度
    sysinfo_platform:'ios',
  }
})
